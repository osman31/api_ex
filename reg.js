
async function fetchregions() {
    const response = await fetch('https://geo.api.gouv.fr/regions');
    const regions = await response.json();
   return regions;
}


fetchregions().then(function(regions) {       
for (let i = 0; i < regions.length; i++) {
    var ul = document.getElementById("liste");
    var li = document.createElement("li");
    li.appendChild(document.createTextNode(regions[i].nom ));
    ul.appendChild(li);
  }
})
